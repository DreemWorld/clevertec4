package vovk.kirill.clevertec4.data_api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BPS (
    @Json(name = "gps_x") val horizontal: String,
    @Json(name = "gps_y") val vertical: String,
    @Json(name = "work_time") val time: String
)
