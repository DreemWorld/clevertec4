package vovk.kirill.clevertec4.data_api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitAdapter {
    suspend fun BPSQuery(): List<BPS> {
        val response = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl("https://belarusbank.by/api/")
            .build()
            .create(BPSQuery::class.java)
            .getBps()

        return if (response.isSuccessful) response.body()!! else emptyList()
    }
}