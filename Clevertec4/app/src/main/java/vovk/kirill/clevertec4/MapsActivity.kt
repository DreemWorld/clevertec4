package vovk.kirill.clevertec4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import vovk.kirill.clevertec4.data_api.BPS
import vovk.kirill.clevertec4.data_api.RetrofitAdapter
import vovk.kirill.clevertec4.databinding.ActivityMapsBinding


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val gomel = LatLng(52.4, 31.0)
        mMap.moveCamera(CameraUpdateFactory.zoomTo(12.5f))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(gomel))

        MainScope().launch {
            val bps: List<BPS> = withContext(Dispatchers.IO) { RetrofitAdapter.BPSQuery() }
            for (atm in bps) {
                mMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(atm.horizontal.toDouble(), atm.vertical.toDouble()))
                        .title(atm.time)
                )
            }
        }
    }
}
