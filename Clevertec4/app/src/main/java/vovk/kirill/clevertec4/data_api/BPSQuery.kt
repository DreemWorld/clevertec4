package vovk.kirill.clevertec4.data_api

import retrofit2.Response
import retrofit2.http.GET

interface BPSQuery {
    @GET ("atm?city=Гомель")
    suspend fun getBps() : Response<List<BPS>>
}